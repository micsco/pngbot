# pngbot

PNG image compression for GitLab merge requests

```yaml
pngbot-commit:
  image: registry.gitlab.com/jramsay/pngbot:v0.1.0
  stage: build
  script:
    - pngbot

pngbot-all:
  image: registry.gitlab.com/jramsay/pngbot:v0.1.0
  stage: build
  script:
    - pngbot -t=all

```

